package sql;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

	private static final String usuario = "root";
	private static final String senha = "d28m03";
	private static final String url = "jdbc:mysql://127.0.0.1:3306/Autentica";

	public static Connection open() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(url, usuario, senha);
		} catch (SQLException | ClassNotFoundException ex) {
			Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}

	}

	public static void close(ResultSet rs, Statement st, Connection conn) {

		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}

		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}

	}
	
	public static void close(Statement st, Connection conn) {
		close(null, st, conn);
	}
	
	public static void close(Connection conn) {
		close(null, null, conn);
	}

}
